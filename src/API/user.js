const API_URL = process.env.REACT_APP_API_URL;
const API_KEY = process.env.REACT_APP_API_KEY;

// Check if user already exists
const checkForUser = async (username) => {
  try {
    const response = await fetch(`${API_URL}?username=${username}`);
    if (!response.ok) {
      throw new Error('checkforuser request failed');
    }
    const data = await response.json();
    return [null, data];
  } catch (error) {
    return [error.message, []];
  }
};

// Create user - Method: POST
const createUser = async (username) => {
  try {
    const response = await fetch(API_URL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'x-api-key': API_KEY,
      },
      body: JSON.stringify({
        username,
        translations: [],
      }),
    });
    if (!response.ok) {
      throw new Error('createUser request failed, username= ' + username);
    }
    const data = await response.json();
    return [null, data];
  } catch (error) {
    return [error.message, []];
  }
};

// Create translation - Method: PATCH
export const createTranslation = async (
  username,
  translation,
  id,
  translations
) => {
  translations.push(translation);
  try {
    const response = await fetch(`${API_URL}/${id}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        'x-api-key': API_KEY,
      },
      body: JSON.stringify({
        translations: translations,
      }),
    });
    if (!response.ok) {
      throw new Error(
        'createTranslation request failed, username= ' + username
      );
    }
    const data = await response.json();
    return [null, data];
  } catch (error) {
    return [error.message, []];
  }
};

// Delete translation - Method: PATCH
export const deleteTranslations = async (username, id) => {
  try {
    const response = await fetch(`${API_URL}/${id}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        'x-api-key': API_KEY,
      },
      body: JSON.stringify({
        translations: [],
      }),
    });
    if (!response.ok) {
      throw new Error(
        'deleteTranslations request failed, username= ' + username
      );
    }
    const data = await response.json();
    return [null, data];
  } catch (error) {
    return [error.message, []];
  }
};

// Login user
export const loginUser = async (username) => {
  const [checkError, user] = await checkForUser(username);
  if (checkError !== null) {
    return [checkError, null];
  }
  if (user.length > 0) {
    return [null, user.pop()];
  }
  return await createUser(username);
};
