import { useState, useEffect, useContext } from 'react';
import { useForm } from 'react-hook-form';
import { loginUser } from '../API/user';
import { storageSave } from '../utils/storage';
import { useNavigate } from 'react-router-dom';
import { UserContext } from '../Contexts/UserProvider';
import { AiOutlineArrowRight } from 'react-icons/ai';
import { FaRegKeyboard } from 'react-icons/fa';

const usernameConfig = {
  required: true,
  minLength: 3,
  maxLength: 40,
};

const LoginForm = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const [user, setUser] = useContext(UserContext);
  const navigate = useNavigate();

  const [loading, setLoading] = useState(false);
  const [apiError, setApiError] = useState(null);

  // Force navigation if user is logged in
  useEffect(() => {
    if (user !== null) {
      navigate('/translate');
    }
  }, [user, navigate]);

  // Log user in and save API response to context API as well as localstorage
  const onSubmit = async ({ username }) => {
    setLoading(true);
    const [error, userResponse] = await loginUser(username);
    if (error !== null) {
      setApiError(error);
    }
    if (userResponse !== null) {
      storageSave('translation-user', userResponse.username);
      setUser(userResponse);
    }

    setLoading(false);
  };

  // Set errormessages
  const errorMessage = (() => {
    if (!errors.username) {
      return null;
    }
    if (errors.username.type === 'required') {
      return <span>username required</span>;
    }
    if (errors.username.type === 'minLength') {
      return <span>minimum username length is 3</span>;
    }
    if (errors.username.type === 'maxLength') {
      return <span>maximum username length is 40</span>;
    }
  })();

  return (
    <>
      <div className='outer-input' id='outer-login-input'>
        <div className='input-left'>
          <FaRegKeyboard size={30} />
          <div id='vertical-line'></div>
        </div>
        <input
          type='text'
          placeholder='Enter username ...'
          className='input'
          {...register('username', usernameConfig)}
          autoComplete='off'
        ></input>
        <button
          type='submit'
          onClick={handleSubmit(onSubmit)}
          className='arrow-button'
          disabled={loading}
        >
          <AiOutlineArrowRight size={30} color={'white'} />
        </button>
      </div>
      {errorMessage}
      {loading && <p>Logging in..</p>}
      {apiError && <p>{apiError}</p>}
    </>
  );
};

export default LoginForm;
