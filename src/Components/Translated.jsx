import React from 'react';
import { validChars } from '../utils/storage';

const Translated = (props) => {
  // Convert props (string) to array of chars
  const destructProps = (text) => {
    let li = [];
    for (let i = 0; i < text.length; i++) {
      li.push(text[i]);
    }
    return li;
  };

  // Check if entered character is valid
  const isValidChar = (char) => {
    return validChars.includes(char.toLowerCase());
  };

  return (
    <div id='translate-box'>
      <div id='translate-image-container'>
        {destructProps(props.text).map((char, i) => {
          return isValidChar(char) ? (
            <img
              src={require('../img/individial_signs/' +
                char.toLowerCase() +
                '.png')}
              key={i}
              alt={char}
              className='translate-image'
            />
          ) : (
            <div key={i}></div>
          );
        })}
      </div>
      <div className='box-bottom'>
        <span id='translate-box-span'>Translation</span>
      </div>
    </div>
  );
};

export default Translated;
