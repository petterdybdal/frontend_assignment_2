import React from 'react';
import { FaUserCircle } from 'react-icons/fa';
import { NavLink } from 'react-router-dom';

const NavButton = (props) => {
  return (
    <NavLink to='/profile' id='profile-nav'>
      <div id='profile-button'>
        <div id='profile-button-name'>{props.name}</div>
        <FaUserCircle size={50} />
      </div>
    </NavLink>
  );
};

export default NavButton;
