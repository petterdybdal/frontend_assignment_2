import React, { useContext } from 'react';
import Logo from '../img/Logo.png';
import NavButton from './NavButton';
import { UserContext } from '../Contexts/UserProvider';
import { NavLink } from 'react-router-dom';
import { storageClear } from '../utils/storage';
import { useNavigate, useLocation } from 'react-router-dom';
import { FiLogOut } from 'react-icons/fi';
import Splash from '../img/Splash.svg';

const NavBar = (props) => {
  const navigate = useNavigate();
  const location = useLocation();

  // Clear localstorage and context data
  const handleLogOut = () => {
    setUser(null);
    storageClear();
    navigate('/');
  };
  const [user, setUser] = useContext(UserContext);
  return (
    <div id='navbar'>
      <NavLink to='/translate' id='translate-nav'>
        <div id='navbar-left'>
          {location.pathname !== '/' && (
            <div id='logo-container'>
              <img src={Splash} alt='splash' id='splash' />
              <img src={Logo} alt='Logo' id='header-image' />
            </div>
          )}
          Lost in Translation
        </div>
      </NavLink>
      <div id='navbar-right'>
        {user && <NavButton name={user.username} />}
        {location.pathname === '/profile' && (
          <button id='logout-button' onClick={handleLogOut}>
            <FiLogOut size={30} />
            <p>Logout</p>
          </button>
        )}
      </div>
    </div>
  );
};

export default NavBar;
