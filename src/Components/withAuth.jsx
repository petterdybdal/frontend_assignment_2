import { UserContext } from '../Contexts/UserProvider';
import { useContext } from 'react';
import { Navigate } from 'react-router-dom';
import { storageClear } from '../utils/storage';

const withAuth = (Component) => (props) => {
  const [user, setUser] = useContext(UserContext);
  if (user !== null) {
    return <Component {...props} />;
  } else {
    storageClear();
    return <Navigate to='/' />;
  }
};

export default withAuth;
