import './App.css';
import NavBar from './Components/NavBar';
import LoginPage from './Pages/LoginPage';
import ProfilePage from './Pages/ProfilePage';
import TranslatorPage from './Pages/TranslatorPage';
import UserProvider from './Contexts/UserProvider';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

function App() {
  return (
    <UserProvider>
      <div className='App'>
        <BrowserRouter>
          <NavBar />
          <Routes>
            <Route path='/' element={<LoginPage />} />
            <Route path='/translate' element={<TranslatorPage />} />
            <Route path='/profile' element={<ProfilePage />} />
          </Routes>
        </BrowserRouter>
      </div>
    </UserProvider>
  );
}

export default App;
