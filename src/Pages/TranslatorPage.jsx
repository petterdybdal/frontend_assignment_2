import React, { useState, useContext } from 'react';
import Translated from '../Components/Translated';
import { FaRegKeyboard } from 'react-icons/fa';
import { AiOutlineArrowRight } from 'react-icons/ai';
import { createTranslation } from '../API/user';
import { UserContext } from '../Contexts/UserProvider';
import withAuth from '../Components/withAuth';

const TranslatorPage = (props) => {
  const [user] = useContext(UserContext);

  const [message, setMessage] = React.useState('');
  const [loading, setLoading] = useState(false);
  const [inputError, setInputError] = useState(false);

  // Ensure the user can't input invalid characters using regex
  const onChangeHandler = (event) => {
    if (!/[^A-Za-z ]/.test(event.target.value)) {
      setMessage(event.target.value);
      setInputError(false);
    } else {
      setInputError(true);
    }
  };

  // Save translation to API
  const saveTranslation = () => {
    setInputError(false);
    setLoading(true);
    createTranslation(user.username, message, user.id, user.translations);
    setTimeout(function () {
      setLoading(false);
    }, 200);
  };

  return (
    <div id='translate-page'>
      <div className='outer-input'>
        <div className='input-left'>
          <FaRegKeyboard size={30} />
          <div id='vertical-line'></div>
        </div>
        <input
          type='text'
          name='name'
          className='input'
          onChange={onChangeHandler}
          value={message}
          autoComplete='off'
          placeholder='Enter phrase to translate ...'
        />
        <button
          className='arrow-button'
          onClick={saveTranslation}
          disabled={loading}
        >
          <AiOutlineArrowRight size={30} color={'white'} />
        </button>
      </div>
      {inputError && <p>Cannot translate numerics and special characters..</p>}
      {loading && <p>Saving translation..</p>}
      <Translated text={message} />
    </div>
  );
};

export default withAuth(TranslatorPage);
