import React from 'react';
import Logo from '../img/Logo.png';
import LoginForm from '../Components/LoginForm';
import Splash from '../img/Splash.svg';

const LoginPage = (props) => {
  return (
    <div id='login-page'>
      <img src={Splash} alt='splash' id='login-splash' />
      <img src={Logo} alt='Logo' id='login-image' />
      <div id='login-text'>
        <h1>Lost in Translation</h1>
        <h2>Get started</h2>
      </div>

      <div id='login-container'>
        <LoginForm />
        <div className='box-bottom' />
      </div>
    </div>
  );
};

export default LoginPage;
