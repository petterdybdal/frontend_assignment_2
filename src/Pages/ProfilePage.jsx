import React from 'react';
import { useContext } from 'react';
import { UserContext } from '../Contexts/UserProvider';
import { deleteTranslations } from '../API/user';
import { AiOutlineDelete } from 'react-icons/ai';
import withAuth from '../Components/withAuth';

const ProfilePage = () => {
  const [user, setUser] = useContext(UserContext);

  // Set translations to empty array in state and API
  const handleDeleteTranslations = () => {
    setUser({ ...user, translations: [] });
    deleteTranslations(user.username, user.id);
  };

  return (
    <div id='profile-page'>
      <div id='profile-page-left'>
        <div id='profile-page-translations'>
          {user.translations.length !== 0 ? (
            <h1>My Translations</h1>
          ) : (
            <h1>You don't have any translations</h1>
          )}
          {user.translations.length
            ? user.translations.slice(-10).map((entry, index) => {
                return (
                  <div key={index} className='profile-translation'>
                    {entry}
                  </div>
                );
              })
            : null}
          <div className='box-bottom' />
        </div>
      </div>
      <div id='profile-page-right'>
        <h1>Profile page for {user.username}</h1>
        <h1>Translations saved: {user.translations.length}</h1>
        <button
          id='clear-translations-button'
          onClick={handleDeleteTranslations}
        >
          <AiOutlineDelete size={25} />
          Delete translations
        </button>
      </div>
    </div>
  );
};

export default withAuth(ProfilePage);
