export const storageSave = (key, value) => {
  localStorage.setItem(key, JSON.stringify(value));
};

export const storageRead = (key) => {
  const data = localStorage.getItem(key);
  if (data) {
    return JSON.stringify(data);
  }
  return null;
};

export const storageClear = () => {
  localStorage.clear();
};

// List of valid characters for translations
export const validChars = [
  'a',
  'b',
  'c',
  'd',
  'e',
  'f',
  'g',
  'h',
  'i',
  'j',
  'k',
  'l',
  'm',
  'n',
  'o',
  'p',
  'q',
  'r',
  's',
  't',
  'u',
  'v',
  'w',
  'x',
  'y',
  'z',
];
