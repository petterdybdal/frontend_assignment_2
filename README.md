# Lost in translation

A sign language translation web application.

## Table of Contents

- [Description](#description)
  - [Login](#login)
  - [Translate](#translate)
  - [Profile](#profile)
- [Component tree](#component-tree)
- [Install](#install)
- [API](#API)
- [Contributors](#contributors)
- [License](#license)

## Description

Lost in translation is an application for translating text into sign language, developed for the second assignment in the front-end part of the Noroff fullstack .NET-course.

The application is a web application that communicates with an API in order to store translations for a given user. The application is divided into three pages: Login, Translate and Profile.

#### Login

A page for "logging" in a given user. The login has no authentication, and simply creates a new user in the database through the API if the username doesn't exist, and if it exists it uses that for storing translations.

#### Translate

Here the user can input text into the text-field and generate sign language for a given phrase. The user can then press the button in order to store the text of the phrase in their profile.

#### Profile

The profile page lets the user view the last 10 of their stored translations. The user can view how many translations they have stored in the database, and can also clear all translations if they wish to. The profile page also includes a button for signing out the user and clearing local storage.

## Component tree

A component tree was designed before starting to develop the project. Although it does not 100% reflect the end-product, the component tree is still a good representation for the hierarchy of the components in the application.

The component tree can be found [here](./Component_tree.pdf).

## Install

This project uses [node](http://nodejs.org) and [npm](https://npmjs.com). Go check them out if you don't have them locally installed.

In order to run the application locally, clone the project with git. Then install dependencies and run the application with npm.

```sh
git clone https://gitlab.com/petterdybdal/frontend_assignment_2
npm install
npm start
```

## Usage

The project is hosted through Vercel at [https://frontend-assignment-2.vercel.app](https://frontend-assignment-2.vercel.app)

The GitLab-repository has a pipeline for the `main` branch to the Vercel-application, meaning that any changes pushed to the `main` branch will automatically trigger a re-deployment of the application.

## API

The API is hosted with [Railway](https://railway.app/) and can be accessed at [https://pd-lost-in-translation-api-production.up.railway.app/](https://pd-lost-in-translation-api-production.up.railway.app/).

In order to perform actions such as deleting, inserting or updating records, an API key is required. Fetching data can be done without an API key. The application hosted on Vercel has the API key stored locally as an environment variable, so the application can be fully tested there.

## Contributors

The application is developed by [Petter Dybdal](https://gitlab.com/petterdybdal) and [Kim-Reino Hjelde](https://gitlab.com/kimreinh).

## License

[MIT](LICENSE)
